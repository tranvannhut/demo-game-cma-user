module.exports = {
  PORT: 5006,
  ENV: '"production"',
  NODE_ENV: '"production"',
  DEBUG_MODE: false,
  BASE_URL_API: '"http://ec2-52-77-221-184.ap-southeast-1.compute.amazonaws.com:4000"',
  HOST: '"http://ec2-52-77-221-184.ap-southeast-1.compute.amazonaws.com:4000"'
};
