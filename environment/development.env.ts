module.exports = {
  PORT: 4005,
  ENV: '"development"',
  NODE_ENV: '"development"',
  DEBUG_MODE: true,
  BASE_URL_API: '"http://ec2-52-77-221-184.ap-southeast-1.compute.amazonaws.com:4000"',
  HOST: '"http://ec2-52-77-221-184.ap-southeast-1.compute.amazonaws.com:4000"'
};
