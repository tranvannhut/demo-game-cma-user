import * as React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import * as express from 'express';
import * as webpackDevMiddleware from 'webpack-dev-middleware';
import * as webpackHotMiddleware from 'webpack-hot-middleware';
import * as webpack from 'webpack';
import { StaticRouter } from 'react-router-dom';
import { matchRoutes, renderRoutes, MatchedRoute } from 'react-router-config';
import { Store } from 'redux';
import * as serialize from 'serialize-javascript';
import { Provider } from 'react-redux';
import 'isomorphic-fetch';
import { HelmetProvider } from 'react-helmet-async';
import * as fs from 'fs';
import * as path from 'path';
import * as cheerio from 'cheerio';

import webpackConfig from '../webpack.config';
import routeConfig from './app/routes';
import { configureStore, IState, actions } from './app/store';
import { isBot } from './bot';

const app: express.Express = express();
const port = process.env.PORT || 4002;
const isProduction = process.env.NODE_ENV === 'production';
let compiler: any;

// hot module replacement
if (!isProduction) {
  const config: webpack.Configuration = webpackConfig(process.env.NODE_ENV as string);
  compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, {
    index: 'index.html',
    publicPath: (config.output as webpack.Output).publicPath as string,
    stats: {
      colors: true,
    },
  }));
  app.use(webpackHotMiddleware(compiler));
}

// needed to serve our application in production
app.use(express.static('./dist/static'));

app.get('*', async (req: express.Request, res: express.Response, next: express.NextFunction) => {
  let html: string = isProduction ? (await fs.readFileSync('./dist/static/main.html', { encoding: 'utf8' })).toString()
    : (await getHtmlDev(compiler)).toString();

  if (!isBot(req.headers['user-agent'])) {
    return res.send(html);
  }
  const store: Store<IState> = configureStore();
  const context: { url?: string } = {};
  let promises: Array<Promise<void>> = [];
  store.dispatch(actions.global.setServerSide(true));
  store.dispatch(actions.global.loadedSplashScreen());

  // fetch async data here
  const matchedRoutes: Array<MatchedRoute<{}>> = matchRoutes<{}>(routeConfig, req.originalUrl);
  for (const { route, match } of matchedRoutes) {
    const component: any = route.component;
    if (component && component.fetchData && typeof component.fetchData === 'function') {
      const promise: Promise<void> = component.fetchData(actions, store, match.params);
      if (typeof promise.then === 'function') {
        promises = [...promises, promise];
      }
    }
  }

  Promise.all(promises).then(() => {
    const helmetContext: any = {};
    const reactAppElement: string = renderToString((
      <HelmetProvider context={helmetContext}>
        <Provider store={store}>
          <StaticRouter location={req.originalUrl} context={context}>
            {renderRoutes(routeConfig)}
          </StaticRouter>
        </Provider>
      </HelmetProvider>
    ));
    const { helmet } = helmetContext;
    // if redirect has been used
    if (context.url) {
      return res.redirect(302, context.url);
    }
    const htmlRendered = renderToStaticMarkup((
      <html>
        <head>
          {helmet.base.toComponent()}
          {helmet.title.toComponent()}
          {helmet.meta.toComponent()}
          {helmet.link.toComponent()}
        </head>
        <body>
          <div id="root" dangerouslySetInnerHTML={{ __html: reactAppElement }} />
          {/* <script src="https://cdn.polyfill.io/v2/polyfill.min.js" /> */}
          <script
            dangerouslySetInnerHTML={{ __html: `window.__REDUX_STATE__=${serialize(store.getState())}` }}
            charSet="UTF-8"
          />
        </body>
      </html>
    ));

    const $ = cheerio.load(htmlRendered.toString());
    html = html.replace(/<head>/g, `<head>${$('head').html()}`);
    html = html.replace(/<div id="root"\/>/g, '');
    html = html.replace(/<body>/g, `<body>${$('body').html()}`);

    return res.send(html);
  }, (err: Error) => res.status(500).send(err.message));
});

app.listen(port, (err: Error) => {
  if (err) {
    throw err;
  }
  // tslint:disable-next-line:no-console
  console.info(`Server listening on ${port}`);
});

function getHtmlDev(c: any): Promise<string> {
  return new Promise((resolve, reject) => {
    const filename = path.join(c.outputPath, 'main.html');
    return c.outputFileSystem.readFile(filename, (err, result) => {
      if (err) { return reject(err); }
      return resolve(result.toString());
    });
  });
}
