import { bindActionCreators, Dispatch } from 'redux';

export interface IOptionBindAllActions {
  includes?: string[];
  excludes?: string[];
}

export function bindAllActionCreators(actions: any, dispatch: Dispatch, opts: IOptionBindAllActions = {}) {
  const result: any = {};
  for (const key in actions) {
    if (opts.includes && opts.includes.includes(key)) {
      result[key] = bindActionCreators<any, any>(actions[key], dispatch);
    } else if (opts.excludes && !opts.excludes.includes(key)) {
      result[key] = bindActionCreators<any, any>(actions[key], dispatch);
    } else {
      result[key] = bindActionCreators<any, any>(actions[key], dispatch);
    }
  }

  return result;
}
