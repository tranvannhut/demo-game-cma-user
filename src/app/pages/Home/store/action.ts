import { httpClient } from 'app/api/http';
import { Game } from 'app/models';
import { crudActions, ICRUDActions } from 'app/store/helpers';
import { IOptionsHTTP } from 'app/api/http/Core';
import { Dispatch } from 'redux';

const { getGameAbacusByGrade, getGameMentalByGrade } = httpClient.game;

const actionBase = crudActions<Game.IModel>('game', null);
const action = {
  ...actionBase,
  getGameAbacus: (opts: IOptionsHTTP) => {
    return async (dispatch: Dispatch) => {
      try {
        dispatch(actionBase.loading());
        const data = await getGameAbacusByGrade(opts);
        dispatch(actionBase.fillDataFilter({
          data,
          pagination: null
        }));
      } catch (error) {
        console.log(error);
      }
      dispatch(actionBase.loaded());
    };
  },
  getGameMental: (opts: IOptionsHTTP) => {
    return async (dispatch: Dispatch) => {
      try {
        dispatch(actionBase.loading());
        const data = await getGameMentalByGrade(opts);
        dispatch(actionBase.fillDataFilter({
          data,
          pagination: null
        }));
      } catch (error) {
        console.log(error);
      }
      dispatch(actionBase.loaded());
    };
  },
};

export interface IGameAction extends ICRUDActions {
  getGameAbacus?: (opts: IOptionsHTTP) => any;
  getGameMental?: (opts: IOptionsHTTP) => any;
}

export const gameAction: IGameAction = action;
