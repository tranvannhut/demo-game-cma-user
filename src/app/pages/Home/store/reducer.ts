import { crudReducers, IInitialState } from 'app/store/helpers';

export const gameReducer = crudReducers<IGameState, any>('game');
export type IGameState = IInitialState;
