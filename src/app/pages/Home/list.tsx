import { Col, Row, Select, Table } from 'antd';
import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Dispatch } from 'redux';

import { actions, IAction, IState } from 'app/store';
import { bindAllActionCreators } from 'app/utils';
import { ColumnProps } from 'antd/lib/table';

export namespace HomePage {
  export interface IProps extends RouteComponentProps<void> {
    state: IState;
    actions: IAction;
    dispatch: Dispatch;
  }

  export interface IStates {
    typeGame: string;
    grade: number;
  }

}

enum TypeGame {
  Abacus = 'abacus',
  Mental = 'mental'
}

const connectT: any = connect(
  (state: IState) => ({ state }),
  (dispatch: Dispatch) => ({
    dispatch,
    actions: bindAllActionCreators(actions, dispatch)
  })
);

@(connectT)

export class HomePage extends React.Component<HomePage.IProps, HomePage.IStates> {

  public grade: Readonly<number[]> = Array.from(new Array(14), (_x, i) => i).reverse();
  public state: Readonly<HomePage.IStates> = {
    grade: this.grade[0],
    typeGame: TypeGame.Abacus
  };

  public columns: Array<ColumnProps<any>> = [{
    title: 'Expression',
    dataIndex: 'expression',
    key: 'expression',
    render: (_t, r) => {
      return `${r.isCurrency ? '$ ' : ''}${r.expression}`;
    }
  }, {
    title: 'Result',
    dataIndex: 'result',
    key: 'result',
    render: (_t, r) => {
      console.log(r);
      return `${r.isCurrency ? '$ ' : ''}${r.result}`;
    }
  }, {
    title: 'Remainder',
    dataIndex: 'remainder',
    key: 'remainder',
  }];

  constructor(props: HomePage.IProps, context?: any) {
    super(props, context);
    this.setTypeGame = this.setTypeGame.bind(this);
    this.setGradeGame = this.setGradeGame.bind(this);
  }

  public render() {
    const { Option } = Select;
    const { data } = this.props.state.game;

    return (
      <div className="list-home">
        <Row>
          <Col md={2}>
            <Select defaultValue={TypeGame.Abacus} onChange={this.setTypeGame}>
              <Option value={TypeGame.Abacus}>Abacus</Option>
              <Option value={TypeGame.Mental}>Mental</Option>
            </Select>
          </Col>
          <Col md={6}>
            <Select defaultValue={this.grade[0]} style={{ width: '120px' }} onChange={this.setGradeGame}>
              {this.grade.map(e => <Option key={e.toString()} value={e}>{e}</Option>)}
            </Select>
          </Col>
        </Row>
        {this.renderGame(data[0] || [], 'Grading')}
        {this.renderGame(data[1] || [], 'Division')}
        {this.renderGame(data[2] || [], 'Multiplication')}
      </div>
    );
  }

  public componentWillMount() {
    this.getGame(null, this.state.grade);
  }

  public setTypeGame(value: TypeGame) {
    this.setState({ typeGame: value });
    this.getGame(value, this.state.grade);
  }

  public setGradeGame(value: number) {
    this.setState({ grade: value });
    this.getGame(null, value);
  }

  public getGame(typeGame?: TypeGame, grade?: number) {
    const funcGame = (typeGame || this.state.typeGame) === TypeGame.Abacus ? 'getGameAbacus' : 'getGameMental';
    this.props.actions.game[funcGame]({ url: { grade } });
  }

  public renderGame(data: any[], name: string) {
    data = data.map((e, i) => ({
      key: i.toString(),
      ...e
    }));
    const loading = this.props.state.game.loading;
    return (
      data.length === 0 && !loading
        ? null
        :
        <Row>
          <Col className="game" md={24}>
            <label className="label-game">{name}</label>
            <Table dataSource={data}
              columns={this.columns}
              loading={this.props.state.game.loading}
              pagination={false} />
          </Col>
        </Row>

    );
  }

}
