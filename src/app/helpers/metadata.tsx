import * as React from 'react';

export enum TypeMetadata {
  Link = 'link',
  Metadata = 'metadata'
}

export interface IMetadataItem {
  type: TypeMetadata;
  content: {
    [key: string]: string
  };
}

export function generateMetadataHeader(items: IMetadataItem[]) {
  return items.map((e, i) => e.type === TypeMetadata.Link ?
    <link key={i} {...e.content} /> : <meta key={i} {...e.content} />);
}
