export interface IPhoneInput {
  phoneCode?: string;
  phoneNumber: string;
}

export enum UpdatedByType {
  Customer = 'customer',
  Staff = 'staff',
  Admin = 'admin',
  System = 'system'
}

export enum StatusCode {
  Active = 'active',
  Inactive = 'inactive',
  Disabled = 'disabled',
  Deleted = 'deleted',
  Deactivate = 'deactivate'
}

export enum CRUDAction {
  Create = 'create',
  Update = 'update',
  Delete = 'delete'
}

export interface IPagingParams {
  page: number;
  size: number;
  order?: string;
  descending?: boolean;
}

export interface IPagingMeta {
  page: number;
  size: number;
  totalPages: number;
  total: number;
}

export interface IPagingResult<T> {
  data: T[];
  pagination: IPagingMeta;
}

export enum Gender {
  Male = 'male',
  Female = 'female'
}

export interface IFileInfo {
  url: string;
  fullPath: string;
  bucketName?: string;
}

export interface IImageUrl {
  url: string;
  thumbnailUrl: string;
}

export const phoneCodeDefault = '+84';
