import { RouteConfig } from 'react-router-config';

import { MainLayout } from './layouts/Main';
import { HomePage } from './pages/Home';

const routeConfig: RouteConfig[] = [
  {
    component: MainLayout,
    routes: [{
      exact: true,
      path: '/',
      component: HomePage
    }]
  },
];

export default routeConfig;
