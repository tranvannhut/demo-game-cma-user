
import { routerReducer } from 'react-router-redux';
import { applyMiddleware, combineReducers, createStore, Store } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import { globalActions } from 'app/store/action';
import { globalReducer } from 'app/store/reducer';
import { IAction, IState } from 'app/store/type';

import { gameAction, gameReducer } from 'app/pages/Home/store';

const reducers = combineReducers<IState>({
  global: globalReducer,
  router: routerReducer,
  game: gameReducer
});

// actions
export const actions: IAction = {
  global: globalActions,
  game: gameAction,
};

export function configureStore(initialState?: IState): Store<IState> {
  return createStore(reducers, initialState as any, getMiddleware()) as Store<IState>;
}

function getMiddleware() {
  let middleware = applyMiddleware(thunkMiddleware);
  if (process.env.DEBUG_MODE === 'true') {
    middleware = applyMiddleware(createLogger({
      actionTransformer: (action: any) => {
        return typeof action === 'function' ? {
          type: 'DISPATCH_ACTION',
        } : action;
      },
    }), thunkMiddleware);
  }

  return middleware;
}

export * from './type';
