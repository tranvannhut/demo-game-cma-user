import { createAction } from 'redux-actions';

export enum TypeActionGlobal {
  LOADING_SPLASH_SCREEN = 'LOADING_SPLASH_SCREEN',
  LOADED_SPLASH_SCREEN = 'LOADED_SPLASH_SCREEN',
  SET_SERVER_SIDE = 'SET_SERVER_SIDE'
}

export const globalActions = {
  loadingSplashScreen: createAction(TypeActionGlobal.LOADING_SPLASH_SCREEN),
  loadedSplashScreen: createAction(TypeActionGlobal.LOADED_SPLASH_SCREEN),
  setServerSide: createAction<boolean>(TypeActionGlobal.SET_SERVER_SIDE)
};

export type IGlobalActions = typeof globalActions;
