import { TypeActionGlobal } from '../store/action';
import { handleActions } from 'redux-actions';
import { IMetadataItem, TypeMetadata } from '../helpers';

export interface IGlobalState {
  loading: boolean;
  isServerSide: boolean;
  metadata: IMetadataItem[];
  title: string;
}

const title = 'Generate Game';

export function metadataDefault(): IMetadataItem[] {
  // tslint:disable-next-line:max-line-length
  const description = '';
  const host = process.env.HOST;
  return [{
    type: TypeMetadata.Link,
    content: {
      rel: 'canonical',
      href: host
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'dc.source',
      content: host
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'dc.creator',
      content: 'nhutdev'
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'description',
      content: description
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'og:title',
      content: title
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'og:description',
      content: description
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'og:url',
      content: host
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'twitter:title',
      content: title
    }
  }, {
    type: TypeMetadata.Metadata,
    content: {
      name: 'twitter:description',
      content: description
    }
  }];
}

export function defaultGlobalState(): IGlobalState {
  return {
    loading: true,
    isServerSide: false,
    metadata: metadataDefault(),
    title
  };
}

export const globalReducer = handleActions<IGlobalState, any>({
  [TypeActionGlobal.LOADING_SPLASH_SCREEN]: (state, _action) => {
    state.loading = true;
    return state;
  },
  [TypeActionGlobal.LOADED_SPLASH_SCREEN]: (state, _action) => {
    state.loading = false;
    return { ...state };
  },
  [TypeActionGlobal.SET_SERVER_SIDE]: (state, action) => {
    state.isServerSide = action.payload;
    return { ...state };
  },
}, defaultGlobalState());
