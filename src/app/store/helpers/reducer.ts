import { parseNameAction, TypeCRUDActions } from 'app/store/helpers/action';
import { handleActions } from 'redux-actions';

export interface IInitialState<M = any> {
  loading: boolean;
  pagination: IPaginationState;
  data: M[];
}

export interface IPaginationState {
  size: number;
  page: number;
  total: number;
}

export function getPaginationStateDefault(): IPaginationState {
  return {
    size: 10,
    page: 1,
    total: 0
  };
}

export function getInitialStateDefault<M>(): IInitialState<M> {
  return {
    loading: false,
    data: [],
    pagination: getPaginationStateDefault()
  };
}

export interface IOptionReducer<S, P> {
  reducers: IReducerExtends<S, P>;
  stateDefault: any;
}

export interface IReducerExtends<S, P> {
  [key: string]: (state: S, action: P) => any;
}

export function crudReducers<S extends any, P>(name: string, keyId = 'id', opts?: IOptionReducer<S, P>) {
  function parseActionType(type: TypeCRUDActions) {
    return parseNameAction(type, name);
  }

  return handleActions<S, P>({
    ...(opts ? opts.reducers : {}) as any,
    [parseActionType(TypeCRUDActions.FILL_DATA_FILTER)]: (state, action: any) => {
      state.pagination = action.payload.pagination;
      state.data = action.payload.data;

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.PAGINATION_CHANGE)]: (state, action: any) => {
      state.pagination = action.payload;

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.LOADED)]: (state, _action) => {
      state.loading = false;

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.LOADING)]: (state, _action) => {
      state.loading = true;

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.REMOVE_MODEL_BY_ID)]: (state, action: any) => {
      const ids: any[] = action.payload.map((e: any) => e.toString());
      state.data = state.data.filter((e: any) => !ids.includes(e[keyId].toString()));

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.PAGINATION_RESET)]: (state, _action) => {
      state.pagination = getPaginationStateDefault();

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.UPDATE_MODELS)]: (state, action) => {
      const models: any = action.payload;
      if (!models || models.length === 0) {
        return { ...state };
      }
      state.data = state.data.map((e: any) => {
        const current = models.find((m: any) => m[keyId].toString() === e[keyId].toString());

        return current ? current : e;
      });

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.REFRESH_MODELS)]: (state, action: any) => {
      state.data = action.payload;

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.ADD_MODEL)]: (state, action: any) => {
      state.data.unshift(action.payload);

      return { ...state };
    },
    [parseActionType(TypeCRUDActions.RESET_STATE)]: () => {
      return {
        ...getInitialStateDefault()
      };
    }
  }, opts ? opts.stateDefault : getInitialStateDefault());
}
