import { IOptionsHTTP } from 'app/api/http/Core';
import { IPagingParams } from 'app/models';
import { IPaginationState } from 'app/store/helpers/reducer';
import * as lodash from 'lodash';
import { AnyAction, Dispatch } from 'redux';
import { createAction } from 'redux-actions';

export interface ICRUDApi<M, F = any> {
  filter?(params: F, pagingParams: any, options?: IOptionsHTTP): Promise<IResultFilter<M>>;
  list?(params?: any, options?: IOptionsHTTP): Promise<M[]>;
  getById?(id: number | string, options?: IOptionsHTTP): Promise<M>;
  create?(form: any, options?: IOptionsHTTP): Promise<M>;
  update?(id: number | string, form: any, options?: IOptionsHTTP): Promise<M>;
  deleteById?(id: number | string, options?: IOptionsHTTP): Promise<M>;
}

export interface ICRUDActionApi<F = any> {
  filter?(params?: F, pagingParams?: any, options?: IOptionsHTTP): AnyAction;
  list?(params?: any, options?: IOptionsHTTP): AnyAction;
  getById?(id: number | string, options?: IOptionsHTTP): AnyAction;
  create?(form: any, options?: IOptionsHTTP): AnyAction;
  update?(id: number | string, form: any, options?: IOptionsHTTP): AnyAction;
  deleteById?(id: number | string, options?: IOptionsHTTP): any;
}

export interface ICRUDActions<M = any, F = any> extends ICRUDActionApi<F> {
  updateModels(payload: M[]): AnyAction;
  fillDataFilter(payload: IResultFilter<M>): AnyAction;
  refreshModels(payload: M[]): any;
  removeModelById(payload: string[] | number[]): AnyAction;
  paginationChange(payload: IPaginationState): AnyAction;
  loading(): AnyAction;
  loaded(): AnyAction;
  resetState(): AnyAction;
}

export enum TypeCRUDActions {
  DELETE_BY_ID = 'DELETE_BY_ID',
  FILL_DATA_FILTER = 'FILL_DATA_FILTER',
  LOADING = 'LOADING',
  LOADED = 'LOADED',
  PAGINATION_CHANGE = 'PAGINATION_CHANGE',
  REMOVE_MODEL_BY_ID = 'REMOVE_MODEL_BY_ID',
  PAGINATION_RESET = 'PAGINATION_RESET',
  UPDATE_MODELS = 'UPDATE_MODELS',
  REFRESH_MODELS = 'REFRESH_MODELS',
  ADD_MODEL = 'ADD_MODEL',
  RESET_STATE = 'RESET_STATE'
}

export interface IResultFilter<M> {
  pagination: IPaginationState;
  data: M[];
}

export interface IExtendAction {
  [key: string]: any;
}

export function parseNameAction(type: TypeCRUDActions, name: string): string {
  return `${name.toUpperCase()}_${type}_CRUD`;
}

export function crudActions<M extends object, F = any>(
  name: string, options: ICRUDApi<M, F>, extendAction?: IExtendAction
):
  ICRUDActions<M, F> {

  const actions = {
    updateModels: createAction<M[]>(parseNameAction(TypeCRUDActions.UPDATE_MODELS, name)),
    addModel: createAction<M>(parseNameAction(TypeCRUDActions.ADD_MODEL, name)),
    fillDataFilter: createAction<IResultFilter<M>>(parseNameAction(TypeCRUDActions.FILL_DATA_FILTER, name)),
    refreshModels: createAction<M[]>(parseNameAction(TypeCRUDActions.REFRESH_MODELS, name)),
    removeModelById: createAction<string[] | number[]>(parseNameAction(TypeCRUDActions.REMOVE_MODEL_BY_ID, name)),
    paginationChange: createAction<IPaginationState>(parseNameAction(TypeCRUDActions.PAGINATION_CHANGE, name)),
    loading: createAction(parseNameAction(TypeCRUDActions.LOADING, name)),
    loaded: createAction(parseNameAction(TypeCRUDActions.LOADED, name)),
    resetState: createAction(parseNameAction(TypeCRUDActions.RESET_STATE, name))
  };

  return {
    ...(extendAction || {}),
    ...actions,
    ...{
      filter: (params?: F, pagingParams?: IPagingParams, opts?: IOptionsHTTP) => {
        return (dispatch: Dispatch, getState: any) => {
          dispatch(actions.loading());

          return options.filter && options.filter(params || null,
            pagingParams || lodash.pick(getState()[name].pagination, ['page', 'size']), opts).then((data) => {
              dispatch(actions.loaded());

              return dispatch(actions.fillDataFilter(data));
            }).catch((error) => {
              console.error(error);
            });
        };
      },
      list: (params?: F, opts?: IOptionsHTTP) => {
        return (dispatch: Dispatch, _getState: any) => {
          dispatch(actions.loading());

          return options.list ? options.list(params, opts).then((data) => {
            dispatch(actions.loaded());

            return dispatch(actions.refreshModels(data));
          }).catch((error) => {
            dispatch(actions.loaded());
            console.error(error);
          }) : undefined;
        };
      },
      getById: (id: string | number) => {
        return (dispatch: Dispatch, _getState: any) => {
          return options.getById ? options.getById(id).then((data) => {
            return dispatch(actions.refreshModels([data]));
          }).catch((error) => {
            console.error(error);
          }) : undefined;
        };
      },
      update: (id: string | number, form: any) => {
        return (dispatch: Dispatch, _getState: any) => {
          return options.update ? options.update(id, form).then((data) => {
            return dispatch(actions.updateModels([data]));
          }).catch((error) => {
            console.error(error);
          }) : undefined;
        };
      },
      create: (form: any, opts?: IOptionsHTTP) => {
        return (dispatch: Dispatch, _getState: any) => {
          return options.create ? options.create(form, opts).then((data) => {
            return dispatch(actions.addModel(data));
          }).catch((error) => {
            console.error(error);
          }) : undefined;
        };
      },
      deleteById: (id: string | number) => {
        return (dispatch: Dispatch, _getState: any) => {
          return options.deleteById ? options.deleteById(id).then(() => {
            return dispatch(actions.removeModelById([id as any]));
          }).catch((error) => {
            console.error(error);
          }) : undefined;
        };
      }
    }
  } as any;
}
