import { RouterState } from 'react-router-redux';

import { IGameState, IGameAction } from 'app/pages/Home/store';
import { IGlobalActions } from 'app/store/action';
import { IGlobalState } from 'app/store/reducer';

export interface IAction {
  global: IGlobalActions;
  game: IGameAction;
}

export interface IState {
  global: IGlobalState;
  router: RouterState;
  game: IGameState;
}
