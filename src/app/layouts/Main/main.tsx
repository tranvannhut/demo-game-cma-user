import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Dispatch } from 'redux';
import { renderRoutes } from 'react-router-config';
import Helmet from 'react-helmet-async';

import { HeaderComponent } from './components';
import { generateMetadataHeader } from 'app/helpers';
import { actions, IAction, IState } from 'app/store';
import { bindAllActionCreators } from 'app/utils';

export namespace MainLayout {
  export interface IProps extends RouteComponentProps<void> {
    children: React.ReactChild;
    state: IState;
    actions: IAction;
    dispatch: Dispatch;
    route: any;
  }
}

@(connect(
  (state: IState) => ({ state }),
  (dispatch: Dispatch) => ({
    dispatch,
    actions: bindAllActionCreators(actions, dispatch)
  })
) as any)

export class MainLayout extends React.Component<MainLayout.IProps> {
  constructor(props: MainLayout.IProps, context?: any) {
    super(props, context);
  }

  public componentWillMount() {
    setTimeout(() => {
      this.props.actions.global.loadedSplashScreen();
    }, 1000);
  }

  public render() {
    const { global: globalState } = this.props.state;
    return (
      <div>
        <Helmet>
          <title>{this.props.state.global.title}</title>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
          {generateMetadataHeader(this.props.state.global.metadata)}
        </Helmet>
        {globalState.loading
          ?
          <div className="spinner">
            <span className="spinner-inner-1" />
            <span className="spinner-inner-2" />
            <span className="spinner-inner-3" />
          </div>
          :
          <div className="layout-main">
            <HeaderComponent location={this.props.location} />
            <div className="layout-main--body">
              {renderRoutes(this.props.route && this.props.route.routes)}
            </div>
          </div >}
      </div>
    );
  }

}
