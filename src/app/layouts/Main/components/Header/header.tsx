import { Layout } from 'antd';
import * as React from 'react';

export namespace HeaderComponent {
  export interface IProps {
    location: any;
  }
}

export class HeaderComponent extends React.Component<HeaderComponent.IProps> {
  constructor(props: HeaderComponent.IProps) {
    super(props);
  }

  public render() {

    return (
      <Layout.Header className="main-header">
        <div className="wrapper">
          <div className="logo">Demo Generate Games</div>
        </div>
      </Layout.Header>
    );
  }

}
