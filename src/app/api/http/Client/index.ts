import { HttpService, IHttpServiceOptions } from '../Core';
import { GameHttpService, IGameHttpService } from './Game';

export interface IHttpClient {
  game: IGameHttpService;
}

export function HttpClient(options: IHttpServiceOptions): IHttpClient {

  const httpService = HttpService(options);

  return {
    game: GameHttpService({ httpService })
  };
}
