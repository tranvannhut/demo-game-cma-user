import { parseParamUrl } from 'app/helpers';
import { Game } from 'app/models';
import { CRUDHttpService, ICRUDHttpService, IHttpServiceClientOptions, IOptionsHTTP } from '../Core';

export interface IGameHttpService extends ICRUDHttpService<Game.IModel> {
  getGameMentalByGrade(opts: IOptionsHTTP): Promise<Game.IModel[]>;
  getGameAbacusByGrade(opts: IOptionsHTTP): Promise<Game.IModel[]>;
}

export function GameHttpService(options: IHttpServiceClientOptions): IGameHttpService {

  const basePath = '/games';
  const abacusUrl = options.httpService.getUrl(`${basePath}/abacus/grades/:grade`);
  const mentalUrl = options.httpService.getUrl(`${basePath}/mental/grades/:grade`);

  function getGameAbacusByGrade(opts: IOptionsHTTP): Promise<any[]> {
    return options.httpService.get(parseParamUrl(abacusUrl, opts.url));
  }

  function getGameMentalByGrade(opts: IOptionsHTTP): Promise<any[]> {
    return options.httpService.get(parseParamUrl(mentalUrl, opts.url));
  }

  return {
    ...CRUDHttpService({
      ...options,
      basePath,
    }),
    getGameAbacusByGrade,
    getGameMentalByGrade
  };
}
