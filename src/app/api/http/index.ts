import { HttpClient } from './Client';

export const httpClient = HttpClient({
  baseURL: process.env.BASE_URL_API,
  mockBaseURL: process.env.MOCK_BASE_URL_API,
  authService: null
});
