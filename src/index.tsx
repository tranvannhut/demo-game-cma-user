import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import { renderRoutes, RouteConfig } from 'react-router-config';
import { BrowserRouter } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';

import Routes from './app/routes';
import { IState, configureStore } from './app/store';

import 'antd/dist/antd.css';
import './assets/styles/index.scss';
import './assets/favicon.ico';

// this is defined with an up-to-date state if we come from a server side rendering context
const store: Store<IState> = configureStore(window.__REDUX_STATE__);

const render: (routes: RouteConfig[]) => void = (routes: RouteConfig[]) => {
    ReactDOM.render(
        <HelmetProvider>
            <AppContainer>
                <Provider store={store}>
                    <BrowserRouter>
                        {renderRoutes(routes)}
                    </BrowserRouter>
                </Provider>
            </AppContainer>
        </HelmetProvider>,
        document.getElementById('root'),
    );
};
render(Routes);

// set rendered to false so newly mounted components can load
// setIsServerSide(store.dispatch, false);

// hot reloading
if (module.hot) {
    // console.log('test', module.hot);
    module.hot.accept('./app/routes', () => {
        // const App: any = require('./app/routes').default;
        render(Routes);
    });
}
