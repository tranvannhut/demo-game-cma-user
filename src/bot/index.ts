import list from './list';
const regex = new RegExp(`(${list.join('|')}')`, 'ig');

export function isBot(userAgent: string) {
  regex.lastIndex = 0;
  return regex.test(userAgent);
}
