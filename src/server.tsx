function loadEnv() {
    const environment = process.env.NODE_ENV || 'development';
    const env = require(`../environment/${environment}.env`);
    Object.keys(env).forEach(e => {
        process.env[e] = JSON.parse(env[e]);
    });

    if (environment === 'production') {
        require('module-alias/register');
    }

    require('./app');
}

loadEnv();
