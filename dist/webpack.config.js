"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var webpack_1 = require("webpack");
// import * as ExtractTextPlugin from 'extract-text-webpack-plugin';
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var UglifyJsPlugin = require("uglifyjs-webpack-plugin");
var OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin");
exports.default = (function (env) {
    var environment = require("./environment/" + (env || 'development') + ".env");
    var isProduction = env === 'production';
    // add hot module replacement if not in production
    var entry = {
        main: './src/index.tsx',
        vendor: [
            'react',
            'react-dom',
            'react-router-dom',
            'react-router-config',
            'redux',
            'react-helmet-async',
            'react-redux',
            'serialize-javascript'
        ]
    };
    entry = isProduction ? entry : __assign({ hot: ['react-hot-loader/patch', 'webpack-hot-middleware/client'] }, entry);
    // set devtool according to the environment
    var devtool = isProduction
        ? 'source-map'
        : 'eval-source-map';
    // : false;
    var plugins = [
        new webpack_1.DefinePlugin({
            'process.env': environment
        }),
        new HtmlWebpackPlugin({
            inject: true,
            filename: 'main.html',
            template: './src/index.html',
            // favicon: helpers.root('/src/favicon.ico'),
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true
            }
        }),
    ];
    // set plugins hot module replacement plugins if not in production
    plugins = isProduction
        ? plugins.concat([
            new MiniCssExtractPlugin({
                filename: 'styles.css'
            }),
            new webpack_1.LoaderOptionsPlugin({
                minimize: true,
                debug: false,
            })
        ]) : plugins.concat([
        new webpack_1.HotModuleReplacementPlugin(),
        new webpack_1.NamedModulesPlugin(),
    ]);
    // const cssRule: Rule = env === 'production' ? {
    //     test: /\.css$/,
    //     use: ExtractTextPlugin.extract(['css-loader', 'postcss-loader']),
    // } : {
    //         test: /\.css$/,
    //         use: ['style-loader', 'css-loader', 'postcss-loader'],
    //     };
    var sassRule = {
        test: /\.(scss|css|sass|less)$/,
        use: isProduction
            ? [
                MiniCssExtractPlugin.loader,
                // 'style-loader',
                'css-loader',
                'sass-loader?sourceMap',
                'postcss-loader'
            ]
            : [
                'style-loader',
                'css-loader',
                'sass-loader?sourceMap',
                'postcss-loader'
            ],
    };
    return {
        entry: entry,
        output: {
            filename: isProduction ? 'main.js' : '[name].js',
            path: path.resolve(__dirname, 'dist', 'static'),
            publicPath: '/',
        },
        node: {
            __dirname: false
        },
        devtool: devtool,
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.css'],
            alias: {
                app: path.resolve(__dirname, 'src', 'app'),
            }
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: ['awesome-typescript-loader'],
                    exclude: /node_modules/,
                },
                // cssRule,
                sassRule,
                {
                    test: /\.(png|ico|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                    loader: 'file-loader?name=[name].[ext]'
                },
            ],
        },
        plugins: plugins,
        mode: isProduction ? 'production' : 'development',
        optimization: isProduction ? {
            removeAvailableModules: true,
            removeEmptyChunks: true,
            mergeDuplicateChunks: true,
            runtimeChunk: 'single',
            splitChunks: {
                chunks: 'all',
                maxInitialRequests: Infinity,
                minSize: 0,
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.s?css$/,
                        chunks: 'all',
                        enforce: true
                    },
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name: function (module) {
                            var packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
                            // npm package names are URL-safe, but some servers don't like @ symbols
                            return "npm." + packageName.replace('@', '');
                        },
                    },
                },
            },
            minimizer: [
                new OptimizeCSSAssetsPlugin(),
                new UglifyJsPlugin()
            ]
        } : {}
    };
});
//# sourceMappingURL=webpack.config.js.map