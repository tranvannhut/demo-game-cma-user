"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UpdatedByType;
(function (UpdatedByType) {
    UpdatedByType["Customer"] = "customer";
    UpdatedByType["Staff"] = "staff";
    UpdatedByType["Admin"] = "admin";
    UpdatedByType["System"] = "system";
})(UpdatedByType = exports.UpdatedByType || (exports.UpdatedByType = {}));
var StatusCode;
(function (StatusCode) {
    StatusCode["Active"] = "active";
    StatusCode["Inactive"] = "inactive";
    StatusCode["Disabled"] = "disabled";
    StatusCode["Deleted"] = "deleted";
    StatusCode["Deactivate"] = "deactivate";
})(StatusCode = exports.StatusCode || (exports.StatusCode = {}));
var CRUDAction;
(function (CRUDAction) {
    CRUDAction["Create"] = "create";
    CRUDAction["Update"] = "update";
    CRUDAction["Delete"] = "delete";
})(CRUDAction = exports.CRUDAction || (exports.CRUDAction = {}));
var Gender;
(function (Gender) {
    Gender["Male"] = "male";
    Gender["Female"] = "female";
})(Gender = exports.Gender || (exports.Gender = {}));
exports.phoneCodeDefault = '+84';
//# sourceMappingURL=Common.js.map