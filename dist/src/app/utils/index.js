"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
function omit(target) {
    var omitKeys = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        omitKeys[_i - 1] = arguments[_i];
    }
    return Object.keys(target).reduce(function (res, key) {
        if (!omitKeys.includes(key)) {
            res[key] = target[key];
        }
        return res;
    }, {});
}
exports.omit = omit;
__export(require("./actions"));
//# sourceMappingURL=index.js.map