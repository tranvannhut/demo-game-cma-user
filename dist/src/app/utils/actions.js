"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
function bindAllActionCreators(actions, dispatch, opts) {
    if (opts === void 0) { opts = {}; }
    var result = {};
    for (var key in actions) {
        if (opts.includes && opts.includes.includes(key)) {
            result[key] = redux_1.bindActionCreators(actions[key], dispatch);
        }
        else if (opts.excludes && !opts.excludes.includes(key)) {
            result[key] = redux_1.bindActionCreators(actions[key], dispatch);
        }
        else {
            result[key] = redux_1.bindActionCreators(actions[key], dispatch);
        }
    }
    return result;
}
exports.bindAllActionCreators = bindAllActionCreators;
//# sourceMappingURL=actions.js.map