"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var action_1 = require("app/store/helpers/action");
var redux_actions_1 = require("redux-actions");
function getPaginationStateDefault() {
    return {
        size: 10,
        page: 1,
        total: 0
    };
}
exports.getPaginationStateDefault = getPaginationStateDefault;
function getInitialStateDefault() {
    return {
        loading: false,
        data: [],
        pagination: getPaginationStateDefault()
    };
}
exports.getInitialStateDefault = getInitialStateDefault;
function crudReducers(name, keyId, opts) {
    if (keyId === void 0) { keyId = 'id'; }
    var _a;
    function parseActionType(type) {
        return action_1.parseNameAction(type, name);
    }
    return redux_actions_1.handleActions(__assign({}, (opts ? opts.reducers : {}), (_a = {}, _a[parseActionType(action_1.TypeCRUDActions.FILL_DATA_FILTER)] = function (state, action) {
        state.pagination = action.payload.pagination;
        state.data = action.payload.data;
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.PAGINATION_CHANGE)] = function (state, action) {
        state.pagination = action.payload;
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.LOADED)] = function (state, _action) {
        state.loading = false;
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.LOADING)] = function (state, _action) {
        state.loading = true;
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.REMOVE_MODEL_BY_ID)] = function (state, action) {
        var ids = action.payload.map(function (e) { return e.toString(); });
        state.data = state.data.filter(function (e) { return !ids.includes(e[keyId].toString()); });
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.PAGINATION_RESET)] = function (state, _action) {
        state.pagination = getPaginationStateDefault();
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.UPDATE_MODELS)] = function (state, action) {
        var models = action.payload;
        if (!models || models.length === 0) {
            return __assign({}, state);
        }
        state.data = state.data.map(function (e) {
            var current = models.find(function (m) { return m[keyId].toString() === e[keyId].toString(); });
            return current ? current : e;
        });
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.REFRESH_MODELS)] = function (state, action) {
        state.data = action.payload;
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.ADD_MODEL)] = function (state, action) {
        state.data.unshift(action.payload);
        return __assign({}, state);
    }, _a[parseActionType(action_1.TypeCRUDActions.RESET_STATE)] = function () {
        return __assign({}, getInitialStateDefault());
    }, _a)), opts ? opts.stateDefault : getInitialStateDefault());
}
exports.crudReducers = crudReducers;
//# sourceMappingURL=reducer.js.map