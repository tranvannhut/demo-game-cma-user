"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var lodash = require("lodash");
var redux_actions_1 = require("redux-actions");
var TypeCRUDActions;
(function (TypeCRUDActions) {
    TypeCRUDActions["DELETE_BY_ID"] = "DELETE_BY_ID";
    TypeCRUDActions["FILL_DATA_FILTER"] = "FILL_DATA_FILTER";
    TypeCRUDActions["LOADING"] = "LOADING";
    TypeCRUDActions["LOADED"] = "LOADED";
    TypeCRUDActions["PAGINATION_CHANGE"] = "PAGINATION_CHANGE";
    TypeCRUDActions["REMOVE_MODEL_BY_ID"] = "REMOVE_MODEL_BY_ID";
    TypeCRUDActions["PAGINATION_RESET"] = "PAGINATION_RESET";
    TypeCRUDActions["UPDATE_MODELS"] = "UPDATE_MODELS";
    TypeCRUDActions["REFRESH_MODELS"] = "REFRESH_MODELS";
    TypeCRUDActions["ADD_MODEL"] = "ADD_MODEL";
    TypeCRUDActions["RESET_STATE"] = "RESET_STATE";
})(TypeCRUDActions = exports.TypeCRUDActions || (exports.TypeCRUDActions = {}));
function parseNameAction(type, name) {
    return name.toUpperCase() + "_" + type + "_CRUD";
}
exports.parseNameAction = parseNameAction;
function crudActions(name, options, extendAction) {
    var actions = {
        updateModels: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.UPDATE_MODELS, name)),
        addModel: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.ADD_MODEL, name)),
        fillDataFilter: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.FILL_DATA_FILTER, name)),
        refreshModels: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.REFRESH_MODELS, name)),
        removeModelById: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.REMOVE_MODEL_BY_ID, name)),
        paginationChange: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.PAGINATION_CHANGE, name)),
        loading: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.LOADING, name)),
        loaded: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.LOADED, name)),
        resetState: redux_actions_1.createAction(parseNameAction(TypeCRUDActions.RESET_STATE, name))
    };
    return __assign({}, (extendAction || {}), actions, {
        filter: function (params, pagingParams, opts) {
            return function (dispatch, getState) {
                dispatch(actions.loading());
                return options.filter && options.filter(params || null, pagingParams || lodash.pick(getState()[name].pagination, ['page', 'size']), opts).then(function (data) {
                    dispatch(actions.loaded());
                    return dispatch(actions.fillDataFilter(data));
                }).catch(function (error) {
                    console.error(error);
                });
            };
        },
        list: function (params, opts) {
            return function (dispatch, _getState) {
                dispatch(actions.loading());
                return options.list ? options.list(params, opts).then(function (data) {
                    dispatch(actions.loaded());
                    return dispatch(actions.refreshModels(data));
                }).catch(function (error) {
                    dispatch(actions.loaded());
                    console.error(error);
                }) : undefined;
            };
        },
        getById: function (id) {
            return function (dispatch, _getState) {
                return options.getById ? options.getById(id).then(function (data) {
                    return dispatch(actions.refreshModels([data]));
                }).catch(function (error) {
                    console.error(error);
                }) : undefined;
            };
        },
        update: function (id, form) {
            return function (dispatch, _getState) {
                return options.update ? options.update(id, form).then(function (data) {
                    return dispatch(actions.updateModels([data]));
                }).catch(function (error) {
                    console.error(error);
                }) : undefined;
            };
        },
        create: function (form, opts) {
            return function (dispatch, _getState) {
                return options.create ? options.create(form, opts).then(function (data) {
                    return dispatch(actions.addModel(data));
                }).catch(function (error) {
                    console.error(error);
                }) : undefined;
            };
        },
        deleteById: function (id) {
            return function (dispatch, _getState) {
                return options.deleteById ? options.deleteById(id).then(function () {
                    return dispatch(actions.removeModelById([id]));
                }).catch(function (error) {
                    console.error(error);
                }) : undefined;
            };
        }
    });
}
exports.crudActions = crudActions;
//# sourceMappingURL=action.js.map