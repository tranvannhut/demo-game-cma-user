"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_router_redux_1 = require("react-router-redux");
var redux_1 = require("redux");
var redux_logger_1 = require("redux-logger");
var redux_thunk_1 = require("redux-thunk");
var action_1 = require("app/store/action");
var reducer_1 = require("app/store/reducer");
var store_1 = require("app/pages/Home/store");
var reducers = redux_1.combineReducers({
    global: reducer_1.globalReducer,
    router: react_router_redux_1.routerReducer,
    game: store_1.gameReducer
});
// actions
exports.actions = {
    global: action_1.globalActions,
    game: store_1.gameAction,
};
function configureStore(initialState) {
    return redux_1.createStore(reducers, initialState, getMiddleware());
}
exports.configureStore = configureStore;
function getMiddleware() {
    var middleware = redux_1.applyMiddleware(redux_thunk_1.default);
    if (process.env.DEBUG_MODE === 'true') {
        middleware = redux_1.applyMiddleware(redux_logger_1.createLogger({
            actionTransformer: function (action) {
                return typeof action === 'function' ? {
                    type: 'DISPATCH_ACTION',
                } : action;
            },
        }), redux_thunk_1.default);
    }
    return middleware;
}
//# sourceMappingURL=index.js.map