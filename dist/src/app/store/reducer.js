"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
var action_1 = require("../store/action");
var redux_actions_1 = require("redux-actions");
var helpers_1 = require("../helpers");
var title = 'Generate Game';
function metadataDefault() {
    // tslint:disable-next-line:max-line-length
    var description = '';
    var host = process.env.HOST;
    return [{
            type: helpers_1.TypeMetadata.Link,
            content: {
                rel: 'canonical',
                href: host
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'dc.source',
                content: host
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'dc.creator',
                content: 'nhutdev'
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'description',
                content: description
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'og:title',
                content: title
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'og:description',
                content: description
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'og:url',
                content: host
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'twitter:title',
                content: title
            }
        }, {
            type: helpers_1.TypeMetadata.Metadata,
            content: {
                name: 'twitter:description',
                content: description
            }
        }];
}
exports.metadataDefault = metadataDefault;
function defaultGlobalState() {
    return {
        loading: true,
        isServerSide: false,
        metadata: metadataDefault(),
        title: title
    };
}
exports.defaultGlobalState = defaultGlobalState;
exports.globalReducer = redux_actions_1.handleActions((_a = {},
    _a[action_1.TypeActionGlobal.LOADING_SPLASH_SCREEN] = function (state, _action) {
        state.loading = true;
        return state;
    },
    _a[action_1.TypeActionGlobal.LOADED_SPLASH_SCREEN] = function (state, _action) {
        state.loading = false;
        return __assign({}, state);
    },
    _a[action_1.TypeActionGlobal.SET_SERVER_SIDE] = function (state, action) {
        state.isServerSide = action.payload;
        return __assign({}, state);
    },
    _a), defaultGlobalState());
//# sourceMappingURL=reducer.js.map