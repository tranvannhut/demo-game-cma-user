"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_actions_1 = require("redux-actions");
var TypeActionGlobal;
(function (TypeActionGlobal) {
    TypeActionGlobal["LOADING_SPLASH_SCREEN"] = "LOADING_SPLASH_SCREEN";
    TypeActionGlobal["LOADED_SPLASH_SCREEN"] = "LOADED_SPLASH_SCREEN";
    TypeActionGlobal["SET_SERVER_SIDE"] = "SET_SERVER_SIDE";
})(TypeActionGlobal = exports.TypeActionGlobal || (exports.TypeActionGlobal = {}));
exports.globalActions = {
    loadingSplashScreen: redux_actions_1.createAction(TypeActionGlobal.LOADING_SPLASH_SCREEN),
    loadedSplashScreen: redux_actions_1.createAction(TypeActionGlobal.LOADED_SPLASH_SCREEN),
    setServerSide: redux_actions_1.createAction(TypeActionGlobal.SET_SERVER_SIDE)
};
//# sourceMappingURL=action.js.map