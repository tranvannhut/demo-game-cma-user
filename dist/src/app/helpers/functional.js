"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function confirm(self, _a) {
    var _this = this;
    var id = _a.id, actionType = _a.actionType, onSuccess = _a.onSuccess;
    self.$msgbox({
        title: self.$t('delete_confirm').toString(),
        message: self.$t('delete_confirm_message').toString(),
        showCancelButton: true,
        confirmButtonText: self.$t('confirm').toString(),
        cancelButtonText: self.$t('cancel').toString(),
        beforeClose: function (action, instance, done) {
            if (action === 'confirm') {
                instance.confirmButtonLoading = true;
                instance.confirmButtonText = 'Loading...';
                self.$store.dispatch(actionType, {
                    id: id,
                    onSuccess: function () {
                        instance.confirmButtonLoading = false;
                        done();
                        onSuccess();
                    },
                    onFailure: function () {
                        done();
                        instance.confirmButtonText = self.$t('confirm').toString();
                        instance.confirmButtonLoading = false;
                    }
                });
            }
            else {
                done();
            }
            return;
        }
    }).then(function () {
        self.$message({
            type: 'info',
            message: _this.$t('delete_successfully').toString()
        });
    }).catch(function () {
        // no handle
    });
}
exports.confirm = confirm;
//# sourceMappingURL=functional.js.map