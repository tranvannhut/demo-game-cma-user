"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash = require("lodash");
function parseParamUrl(url, paramUrl) {
    var result = lodash.cloneDeep(url);
    if (paramUrl) {
        var keys = Object.keys(paramUrl);
        keys.forEach(function (e) {
            var reg = new RegExp(":" + e, 'g');
            result = result.replace(reg, paramUrl[e]);
        });
    }
    return result;
}
exports.parseParamUrl = parseParamUrl;
//# sourceMappingURL=http.js.map