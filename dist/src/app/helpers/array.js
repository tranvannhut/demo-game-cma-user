"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function swapMap(a, i, j) {
    return a.map(function (v, k) {
        switch (k) {
            case i: return a[j];
            case j: return a[i];
            default: return v;
        }
    });
}
exports.swapMap = swapMap;
//# sourceMappingURL=array.js.map