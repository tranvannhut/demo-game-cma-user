"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
function defaultValue(obj, value) {
    if (value === void 0) { value = {}; }
    var result = {};
    obj = lodash_1.cloneDeep(obj);
    function progress(input, r) {
        if (r === void 0) { r = {}; }
        Object.keys(input).forEach(function (e) {
            // let valueCurrent;
            var current = input[e];
            // type = typeof input[e];
            // if (Array.isArray(current)) {
            //   r[e] = [];
            // } else if (current instanceof Date) {
            //   r[e] = null;
            // } else if (type === 'object' && current !== null) {
            //   r[e] = progress(input[e], r[e]);
            // } else {
            //   if (type === 'string') {
            //     valueCurrent = '';
            //   } else if (type === 'number') {
            //     valueCurrent = null;
            //   } else {
            //     valueCurrent = '';
            //   }
            //   r[e] = valueCurrent;
            // }
            switch (true) {
                case Array.isArray(current):
                    r[e] = [];
                    break;
                case current instanceof Date:
                    r[e] = null;
                    break;
                case (lodash_1.isObject(current) && !lodash_1.isNull(current)):
                    r[e] = progress(input[e], r[e]);
                    break;
                case lodash_1.isString(current):
                    r[e] = '';
                    break;
                case lodash_1.isNumber(current):
                    r[e] = null;
                    break;
                case lodash_1.isBoolean(current):
                    r[e] = false;
                default:
                    break;
            }
        });
        return r;
    }
    return __assign({}, progress(obj, result), value);
}
exports.defaultValue = defaultValue;
function removeEmptyField(obj) {
    // const result = obj;
    obj = lodash_1.cloneDeep(obj);
    function progress(input) {
        Object.keys(input).forEach(function (e) {
            var current = input[e], type = typeof input[e];
            if (e === 'id') {
                delete obj[e];
            }
            if (Array.isArray(current)) {
                if (current.length === 0) {
                    delete obj[e];
                }
            }
            else if (type === 'string') {
                if (current.trim() === '') {
                    delete obj[e];
                }
                return;
            }
            else if (current === null) {
                delete obj[e];
            }
        });
        return obj;
    }
    return __assign({}, progress(obj));
}
exports.removeEmptyField = removeEmptyField;
function isEmptyObject(input) {
    var keys = Object.keys(input);
    var arrayValueEmpty = [null, undefined, ''];
    if (keys.length === 0) {
        return true;
    }
    var check = keys.map(function (e) {
        return arrayValueEmpty.indexOf(input[e]) > -1;
    });
    return check.filter(function (e) { return e === true; }).length > 0 ? true : false;
}
exports.isEmptyObject = isEmptyObject;
//# sourceMappingURL=object.js.map