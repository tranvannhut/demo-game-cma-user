"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var TypeMetadata;
(function (TypeMetadata) {
    TypeMetadata["Link"] = "link";
    TypeMetadata["Metadata"] = "metadata";
})(TypeMetadata = exports.TypeMetadata || (exports.TypeMetadata = {}));
function generateMetadataHeader(items) {
    return items.map(function (e, i) { return e.type === TypeMetadata.Link ?
        React.createElement("link", __assign({ key: i }, e.content)) : React.createElement("meta", __assign({ key: i }, e.content)); });
}
exports.generateMetadataHeader = generateMetadataHeader;
//# sourceMappingURL=metadata.js.map