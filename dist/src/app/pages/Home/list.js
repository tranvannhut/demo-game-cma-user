"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var antd_1 = require("antd");
var React = require("react");
var react_redux_1 = require("react-redux");
var store_1 = require("app/store");
var utils_1 = require("app/utils");
var TypeGame;
(function (TypeGame) {
    TypeGame["Abacus"] = "abacus";
    TypeGame["Mental"] = "mental";
})(TypeGame || (TypeGame = {}));
var connectT = react_redux_1.connect(function (state) { return ({ state: state }); }, function (dispatch) { return ({
    dispatch: dispatch,
    actions: utils_1.bindAllActionCreators(store_1.actions, dispatch)
}); });
var HomePage = /** @class */ (function (_super) {
    __extends(HomePage, _super);
    function HomePage(props, context) {
        var _this = _super.call(this, props, context) || this;
        _this.grade = Array.from(new Array(14), function (_x, i) { return i; }).reverse();
        _this.state = {
            grade: _this.grade[0],
            typeGame: TypeGame.Abacus
        };
        _this.columns = [{
                title: 'Expression',
                dataIndex: 'expression',
                key: 'expression',
                render: function (_t, r) {
                    return "" + (r.isCurrency ? '$ ' : '') + r.expression;
                }
            }, {
                title: 'Result',
                dataIndex: 'result',
                key: 'result',
                render: function (_t, r) {
                    console.log(r);
                    return "" + (r.isCurrency ? '$ ' : '') + r.result;
                }
            }, {
                title: 'Remainder',
                dataIndex: 'remainder',
                key: 'remainder',
            }];
        _this.setTypeGame = _this.setTypeGame.bind(_this);
        _this.setGradeGame = _this.setGradeGame.bind(_this);
        return _this;
    }
    HomePage.prototype.render = function () {
        var Option = antd_1.Select.Option;
        var data = this.props.state.game.data;
        return (React.createElement("div", { className: "list-home" },
            React.createElement(antd_1.Row, null,
                React.createElement(antd_1.Col, { md: 2 },
                    React.createElement(antd_1.Select, { defaultValue: TypeGame.Abacus, onChange: this.setTypeGame },
                        React.createElement(Option, { value: TypeGame.Abacus }, "Abacus"),
                        React.createElement(Option, { value: TypeGame.Mental }, "Mental"))),
                React.createElement(antd_1.Col, { md: 6 },
                    React.createElement(antd_1.Select, { defaultValue: this.grade[0], style: { width: '120px' }, onChange: this.setGradeGame }, this.grade.map(function (e) { return React.createElement(Option, { key: e.toString(), value: e }, e); })))),
            this.renderGame(data[0] || [], 'Grading'),
            this.renderGame(data[1] || [], 'Division'),
            this.renderGame(data[2] || [], 'Multiplication')));
    };
    HomePage.prototype.componentWillMount = function () {
        this.getGame(null, this.state.grade);
    };
    HomePage.prototype.setTypeGame = function (value) {
        this.setState({ typeGame: value });
        this.getGame(value, this.state.grade);
    };
    HomePage.prototype.setGradeGame = function (value) {
        this.setState({ grade: value });
        this.getGame(null, value);
    };
    HomePage.prototype.getGame = function (typeGame, grade) {
        var funcGame = (typeGame || this.state.typeGame) === TypeGame.Abacus ? 'getGameAbacus' : 'getGameMental';
        this.props.actions.game[funcGame]({ url: { grade: grade } });
    };
    HomePage.prototype.renderGame = function (data, name) {
        data = data.map(function (e, i) { return (__assign({ key: i.toString() }, e)); });
        var loading = this.props.state.game.loading;
        return (data.length === 0 && !loading
            ? null
            :
                React.createElement(antd_1.Row, null,
                    React.createElement(antd_1.Col, { className: "game", md: 24 },
                        React.createElement("label", { className: "label-game" }, name),
                        React.createElement(antd_1.Table, { dataSource: data, columns: this.columns, loading: this.props.state.game.loading, pagination: false }))));
    };
    HomePage = __decorate([
        (connectT)
    ], HomePage);
    return HomePage;
}(React.Component));
exports.HomePage = HomePage;
//# sourceMappingURL=list.js.map