"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Client_1 = require("./Client");
exports.httpClient = Client_1.HttpClient({
    baseURL: process.env.BASE_URL_API,
    mockBaseURL: process.env.MOCK_BASE_URL_API,
    authService: null
});
//# sourceMappingURL=index.js.map