"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Core_1 = require("../Core");
var Game_1 = require("./Game");
function HttpClient(options) {
    var httpService = Core_1.HttpService(options);
    return {
        game: Game_1.GameHttpService({ httpService: httpService })
    };
}
exports.HttpClient = HttpClient;
//# sourceMappingURL=index.js.map