"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = require("app/helpers");
var Core_1 = require("../Core");
function GameHttpService(options) {
    var basePath = '/games';
    var abacusUrl = options.httpService.getUrl(basePath + "/abacus/grades/:grade");
    var mentalUrl = options.httpService.getUrl(basePath + "/mental/grades/:grade");
    function getGameAbacusByGrade(opts) {
        return options.httpService.get(helpers_1.parseParamUrl(abacusUrl, opts.url));
    }
    function getGameMentalByGrade(opts) {
        return options.httpService.get(helpers_1.parseParamUrl(mentalUrl, opts.url));
    }
    return __assign({}, Core_1.CRUDHttpService(__assign({}, options, { basePath: basePath })), { getGameAbacusByGrade: getGameAbacusByGrade,
        getGameMentalByGrade: getGameMentalByGrade });
}
exports.GameHttpService = GameHttpService;
//# sourceMappingURL=Game.js.map