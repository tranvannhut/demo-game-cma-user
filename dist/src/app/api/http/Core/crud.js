"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = require("app/helpers");
function CRUDHttpService(options) {
    var basePath = options.basePath, httpService = options.httpService, mock = options.mock;
    var baseURL = httpService.getUrl(basePath, mock);
    function filter(params, pagingParams, opts) {
        return httpService.get(helpers_1.parseParamUrl(baseURL, opts ? opts.url : null), {
            params: __assign({}, params, pagingParams, { order: !pagingParams || !pagingParams.order ? undefined
                    : (pagingParams.descending ? '-' + pagingParams.order : pagingParams.order) })
        });
    }
    function getById(id, opts) {
        return httpService.get(helpers_1.parseParamUrl(baseURL, opts ? opts.url : null) + "/" + id);
    }
    function list(opts) {
        return httpService.get(helpers_1.parseParamUrl(baseURL, opts ? opts.url : null));
    }
    function create(form, opts) {
        return httpService.post(helpers_1.parseParamUrl(baseURL, opts ? opts.url : null), form);
    }
    function update(id, form, opts) {
        return httpService.put(helpers_1.parseParamUrl(baseURL, opts ? opts.url : null) + "/" + id, form);
    }
    function deleteById(id, opts) {
        return httpService.del(helpers_1.parseParamUrl(baseURL, opts ? opts.url : null) + "/" + id);
    }
    return {
        filter: filter,
        getById: getById,
        create: create,
        update: update,
        deleteById: deleteById,
        list: list
    };
}
exports.CRUDHttpService = CRUDHttpService;
//# sourceMappingURL=crud.js.map