"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_redux_1 = require("react-redux");
var react_router_config_1 = require("react-router-config");
var react_helmet_async_1 = require("react-helmet-async");
var components_1 = require("./components");
var helpers_1 = require("app/helpers");
var store_1 = require("app/store");
var utils_1 = require("app/utils");
var MainLayout = /** @class */ (function (_super) {
    __extends(MainLayout, _super);
    function MainLayout(props, context) {
        return _super.call(this, props, context) || this;
    }
    MainLayout.prototype.componentWillMount = function () {
        var _this = this;
        setTimeout(function () {
            _this.props.actions.global.loadedSplashScreen();
        }, 1000);
    };
    MainLayout.prototype.render = function () {
        var globalState = this.props.state.global;
        return (React.createElement("div", null,
            React.createElement(react_helmet_async_1.default, null,
                React.createElement("title", null, this.props.state.global.title),
                React.createElement("meta", { "http-equiv": "Content-Type", content: "text/html; charset=utf-8" }),
                React.createElement("meta", { name: "viewport", content: "width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" }),
                helpers_1.generateMetadataHeader(this.props.state.global.metadata)),
            globalState.loading
                ?
                    React.createElement("div", { className: "spinner" },
                        React.createElement("span", { className: "spinner-inner-1" }),
                        React.createElement("span", { className: "spinner-inner-2" }),
                        React.createElement("span", { className: "spinner-inner-3" }))
                :
                    React.createElement("div", { className: "layout-main" },
                        React.createElement(components_1.HeaderComponent, { location: this.props.location }),
                        React.createElement("div", { className: "layout-main--body" }, react_router_config_1.renderRoutes(this.props.route && this.props.route.routes)))));
    };
    MainLayout = __decorate([
        react_redux_1.connect(function (state) { return ({ state: state }); }, function (dispatch) { return ({
            dispatch: dispatch,
            actions: utils_1.bindAllActionCreators(store_1.actions, dispatch)
        }); })
    ], MainLayout);
    return MainLayout;
}(React.Component));
exports.MainLayout = MainLayout;
//# sourceMappingURL=main.js.map