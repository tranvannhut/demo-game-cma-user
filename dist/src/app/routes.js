"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Main_1 = require("./layouts/Main");
var Home_1 = require("./pages/Home");
var routeConfig = [
    {
        component: Main_1.MainLayout,
        routes: [{
                exact: true,
                path: '/',
                component: Home_1.HomePage
            }]
    },
];
exports.default = routeConfig;
//# sourceMappingURL=routes.js.map