"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var server_1 = require("react-dom/server");
var express = require("express");
var webpackDevMiddleware = require("webpack-dev-middleware");
var webpackHotMiddleware = require("webpack-hot-middleware");
var webpack = require("webpack");
var react_router_dom_1 = require("react-router-dom");
var react_router_config_1 = require("react-router-config");
var serialize = require("serialize-javascript");
var react_redux_1 = require("react-redux");
require("isomorphic-fetch");
var react_helmet_async_1 = require("react-helmet-async");
var fs = require("fs");
var path = require("path");
var cheerio = require("cheerio");
var webpack_config_1 = require("../webpack.config");
var routes_1 = require("./app/routes");
var store_1 = require("./app/store");
var bot_1 = require("./bot");
var app = express();
var port = process.env.PORT || 4002;
var isProduction = process.env.NODE_ENV === 'production';
var compiler;
// hot module replacement
if (!isProduction) {
    var config = webpack_config_1.default(process.env.NODE_ENV);
    compiler = webpack(config);
    app.use(webpackDevMiddleware(compiler, {
        index: 'index.html',
        publicPath: config.output.publicPath,
        stats: {
            colors: true,
        },
    }));
    app.use(webpackHotMiddleware(compiler));
}
// needed to serve our application in production
app.use(express.static('./dist/static'));
app.get('*', function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var html, _a, store, context, promises, matchedRoutes, _i, matchedRoutes_1, _b, route, match, component, promise;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                if (!isProduction) return [3 /*break*/, 2];
                return [4 /*yield*/, fs.readFileSync('./dist/static/main.html', { encoding: 'utf8' })];
            case 1:
                _a = (_c.sent()).toString();
                return [3 /*break*/, 4];
            case 2: return [4 /*yield*/, getHtmlDev(compiler)];
            case 3:
                _a = (_c.sent()).toString();
                _c.label = 4;
            case 4:
                html = _a;
                if (!bot_1.isBot(req.headers['user-agent'])) {
                    return [2 /*return*/, res.send(html)];
                }
                store = store_1.configureStore();
                context = {};
                promises = [];
                store.dispatch(store_1.actions.global.setServerSide(true));
                store.dispatch(store_1.actions.global.loadedSplashScreen());
                matchedRoutes = react_router_config_1.matchRoutes(routes_1.default, req.originalUrl);
                for (_i = 0, matchedRoutes_1 = matchedRoutes; _i < matchedRoutes_1.length; _i++) {
                    _b = matchedRoutes_1[_i], route = _b.route, match = _b.match;
                    component = route.component;
                    if (component && component.fetchData && typeof component.fetchData === 'function') {
                        promise = component.fetchData(store_1.actions, store, match.params);
                        if (typeof promise.then === 'function') {
                            promises = promises.concat([promise]);
                        }
                    }
                }
                Promise.all(promises).then(function () {
                    var helmetContext = {};
                    var reactAppElement = server_1.renderToString((React.createElement(react_helmet_async_1.HelmetProvider, { context: helmetContext },
                        React.createElement(react_redux_1.Provider, { store: store },
                            React.createElement(react_router_dom_1.StaticRouter, { location: req.originalUrl, context: context }, react_router_config_1.renderRoutes(routes_1.default))))));
                    var helmet = helmetContext.helmet;
                    // if redirect has been used
                    if (context.url) {
                        return res.redirect(302, context.url);
                    }
                    var htmlRendered = server_1.renderToStaticMarkup((React.createElement("html", null,
                        React.createElement("head", null,
                            helmet.base.toComponent(),
                            helmet.title.toComponent(),
                            helmet.meta.toComponent(),
                            helmet.link.toComponent()),
                        React.createElement("body", null,
                            React.createElement("div", { id: "root", dangerouslySetInnerHTML: { __html: reactAppElement } }),
                            React.createElement("script", { dangerouslySetInnerHTML: { __html: "window.__REDUX_STATE__=" + serialize(store.getState()) }, charSet: "UTF-8" })))));
                    var $ = cheerio.load(htmlRendered.toString());
                    html = html.replace(/<head>/g, "<head>" + $('head').html());
                    html = html.replace(/<div id="root"\/>/g, '');
                    html = html.replace(/<body>/g, "<body>" + $('body').html());
                    return res.send(html);
                }, function (err) { return res.status(500).send(err.message); });
                return [2 /*return*/];
        }
    });
}); });
app.listen(port, function (err) {
    if (err) {
        throw err;
    }
    // tslint:disable-next-line:no-console
    console.info("Server listening on " + port);
});
function getHtmlDev(c) {
    return new Promise(function (resolve, reject) {
        var filename = path.join(c.outputPath, 'main.html');
        return c.outputFileSystem.readFile(filename, function (err, result) {
            if (err) {
                return reject(err);
            }
            return resolve(result.toString());
        });
    });
}
//# sourceMappingURL=app.js.map