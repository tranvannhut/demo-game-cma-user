"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var list_1 = require("./list");
var regex = new RegExp("(" + list_1.default.join('|') + "')", 'ig');
function isBot(userAgent) {
    regex.lastIndex = 0;
    return regex.test(userAgent);
}
exports.isBot = isBot;
//# sourceMappingURL=index.js.map