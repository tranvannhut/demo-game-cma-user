function loadEnv() {
    var environment = process.env.NODE_ENV || 'development';
    var env = require("../environment/" + environment + ".env");
    Object.keys(env).forEach(function (e) {
        process.env[e] = JSON.parse(env[e]);
    });
    if (environment === 'production') {
        require('module-alias/register');
    }
    require('./app');
}
loadEnv();
//# sourceMappingURL=server.js.map