import * as path from 'path';
import {
    Configuration,
    // optimize,
    HotModuleReplacementPlugin,
    NamedModulesPlugin,
    Entry,
    DefinePlugin,
    Plugin,
    LoaderOptionsPlugin,
    Rule,
} from 'webpack';
// import * as ExtractTextPlugin from 'extract-text-webpack-plugin';
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin';
import * as UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import * as OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';

export default (env: string): Configuration => {

    const environment = require(`./environment/${env || 'development'}.env`);
    const isProduction = env === 'production';
    // add hot module replacement if not in production
    let entry: Entry = {
        main: './src/index.tsx',
        vendor: [
            'react',
            'react-dom',
            'react-router-dom',
            'react-router-config',
            'redux',
            'react-helmet-async',
            'react-redux',
            'serialize-javascript'
        ]
    };
    entry = isProduction ? entry : {
        hot: ['react-hot-loader/patch', 'webpack-hot-middleware/client'],
        ...entry,
    };
    // set devtool according to the environment
    const devtool: 'source-map' | 'eval-source-map' | boolean = isProduction
        ? 'source-map'
        : 'eval-source-map';
    // : false;

    let plugins: Plugin[] = [
        new DefinePlugin({
            'process.env': environment
        }),
        new HtmlWebpackPlugin({
            inject: true,
            filename: 'main.html',
            template: './src/index.html',
            // favicon: helpers.root('/src/favicon.ico'),
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true
            }
        }),
    ];
    // set plugins hot module replacement plugins if not in production
    plugins = isProduction
        ? [
            ...plugins,
            new MiniCssExtractPlugin({
                filename: 'styles.css'
            }),
            new LoaderOptionsPlugin({
                minimize: true,
                debug: false,
            })
        ]
        : [
            ...plugins,
            new HotModuleReplacementPlugin(),
            new NamedModulesPlugin(),
        ];
    // const cssRule: Rule = env === 'production' ? {
    //     test: /\.css$/,
    //     use: ExtractTextPlugin.extract(['css-loader', 'postcss-loader']),
    // } : {
    //         test: /\.css$/,
    //         use: ['style-loader', 'css-loader', 'postcss-loader'],
    //     };

    const sassRule: Rule = {
        test: /\.(scss|css|sass|less)$/,
        use: isProduction
            ? [
                MiniCssExtractPlugin.loader,
                // 'style-loader',
                'css-loader',
                'sass-loader?sourceMap',
                'postcss-loader'
            ]
            : [
                'style-loader',
                'css-loader',
                'sass-loader?sourceMap',
                'postcss-loader'
            ],
    };

    return {
        entry,
        output: {
            filename: isProduction ? 'main.js' : '[name].js',
            path: path.resolve(__dirname, 'dist', 'static'),
            publicPath: '/',
        },
        node: {
            __dirname: false
        },
        devtool,
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.css'],
            alias: {
                app: path.resolve(__dirname, 'src', 'app'),
            }
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: ['awesome-typescript-loader'],
                    exclude: /node_modules/,
                },
                // cssRule,
                sassRule,
                {
                    test: /\.(png|ico|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                    loader: 'file-loader?name=[name].[ext]'
                },
            ],
        },
        plugins,
        mode: isProduction ? 'production' : 'development',
        optimization: isProduction ? {
            removeAvailableModules: true,
            removeEmptyChunks: true,
            mergeDuplicateChunks: true,
            runtimeChunk: 'single',
            splitChunks: {
                chunks: 'all',
                maxInitialRequests: Infinity,
                minSize: 0,
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.s?css$/,
                        chunks: 'all',
                        enforce: true
                    },
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name(module) {
                            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
                            // npm package names are URL-safe, but some servers don't like @ symbols
                            return `npm.${packageName.replace('@', '')}`;
                        },
                    },
                },
            },

            minimizer: [
                new OptimizeCSSAssetsPlugin(),
                new UglifyJsPlugin()
            ]
        } : {}
    };
};
